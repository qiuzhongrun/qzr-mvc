package qiuzhongrun.com.annotation;

import java.lang.annotation.*;

/**
 * AOP 的 切面 注解
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE_USE)
public @interface MethodLog {
}