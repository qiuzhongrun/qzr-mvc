package qiuzhongrun.com.annotation;

import qiuzhongrun.com.enumeration.HttpRequestType;

import java.lang.annotation.*;

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface RestMapping {
    public String api();
    public HttpRequestType type();
}
