package qiuzhongrun.com.aspect;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * refer to imooc
 * @link https://www.imooc.com/article/23082?block_id=tuijian_wz
 */
public class ControllerLogInvocationHandler implements InvocationHandler {
    private Object target;

    public ControllerLogInvocationHandler(Object target) {
        this.target = target;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        System.out.println("before-------切面加入逻辑  method = [" + (method==null?"null":"not null") + "], args = [" + args + "]");
        Object invoke = method.invoke(target, args);
        System.out.println("after-------切面加入逻辑---"+invoke);
        return invoke;
    }
}
