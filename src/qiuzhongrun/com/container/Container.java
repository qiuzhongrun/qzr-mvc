package qiuzhongrun.com.container;

import qiuzhongrun.com.annotation.*;
import qiuzhongrun.com.aspect.ControllerLogInvocationHandler;
import qiuzhongrun.com.controller.BaseController;
import qiuzhongrun.com.enumeration.HttpRequestType;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

public class Container {
    private Map<String, Object> CLASS_OBJECT_CONTAINER = new ConcurrentHashMap<>();
    /*
    HttpRequestType
        api
            method  classPath
     */
    private Map<HttpRequestType, Map<String, Map<Method, String>>> HTTP_API_CONTAINER = new ConcurrentHashMap<>();

    /**
     * 目前只做了几个简单的注解的初始化，很多场景都还没有考虑
     * @param classes
     * @param <T>
     */
    public <T> void init(Set<Class> classes) {
        Map<String, Class<T>> annotaionClassMap = new ConcurrentHashMap<>();
        Map<String, Class<T>> annotaionAopClassMap = new ConcurrentHashMap<>();
        classes.forEach(clazz->{
            Annotation[] annotations = clazz.getAnnotations();
            if (!clazz.isAnnotation() && annotations != null && annotations.length > 0) {
                if (clazz.isAnnotationPresent(Controller.class) ||  clazz.isAnnotationPresent(Service.class)) {
                    annotaionClassMap.put(clazz.getName(), clazz);
                }

                if (clazz.isAnnotationPresent(MethodLog.class)) {
                    annotaionAopClassMap.put(clazz.getName(), clazz);
                }

            }
        });

        pipleAssembling(annotaionClassMap, annotaionAopClassMap);
        System.out.println("annotaionClass = [" + annotaionClassMap + "]");
    }

    /**
     * 假装自己在实现一个管道、、、
     * @param annotaionClassMap
     * @param <T>
     */
    private <T> void pipleAssembling(Map<String, Class<T>> annotaionClassMap, Map<String, Class<T>> annotaionAopClassMap) {
        instance(annotaionClassMap);
        field(annotaionClassMap);
        methd(annotaionClassMap);
        aop(annotaionAopClassMap);
    }

    private <T> void aop(Map<String, Class<T>> annotaionAopClassMap) {
        annotaionAopClassMap.forEach((classPath, clazz)->{
             Object obj = CLASS_OBJECT_CONTAINER.get(classPath);
             if (obj == null) {
                 try {
                     obj = clazz.newInstance();
                 } catch (InstantiationException e) {
                     e.printStackTrace();
                 } catch (IllegalAccessException e) {
                     e.printStackTrace();
                 }
             }

            ControllerLogInvocationHandler handler = new ControllerLogInvocationHandler(obj);
            BaseController proxyInstance = (BaseController) Proxy.newProxyInstance(
                    obj.getClass().getClassLoader(),
                    obj.getClass().getInterfaces(),
                    handler
            );

            CLASS_OBJECT_CONTAINER.put(classPath, proxyInstance);
        });
    }

    /**
     * 解析api值并存储起来预备http请求到来的时候提供访问
     * @param annotaionClassMap
     * @param <T>
     */
    private <T> void methd(Map<String, Class<T>> annotaionClassMap) {
        annotaionClassMap.forEach((classPath, clazz)->{
            Method[] methods = clazz.getDeclaredMethods();
            if (methods!=null) {
                for (int i = 0; i < methods.length; i++) {
                    Method method = methods[i];
                    if (method.isAnnotationPresent(RestMapping.class)) {
                        RestMapping annotation = method.getAnnotation(RestMapping.class);//假装只有一个注解、、、
                        String api = annotation.api();
                        HttpRequestType type = annotation.type();

                        put2Container(api, type, method, classPath);
                    }
                }
            }
        });
    }

    private void put2Container(String api, HttpRequestType type, Method method, String classPath) {
        if (!api.startsWith("/")) {
            api = "/"+api;
        }

        Map<String, Map<Method, String>> apiMethodObjectMap = HTTP_API_CONTAINER.get(type);
        if ( null == apiMethodObjectMap ) {
            apiMethodObjectMap = new ConcurrentHashMap<>();
            HTTP_API_CONTAINER.put(type, apiMethodObjectMap);
        }

        Map<Method, String> methodObjectMap = apiMethodObjectMap.get(api);
        if ( null == methodObjectMap ) {
            methodObjectMap = new ConcurrentHashMap<>();
            apiMethodObjectMap.put(api, methodObjectMap);
        }

        methodObjectMap.put(method, classPath);
    }

    /**
     * 填充属性值
     * @param annotaionClassMap
     * @param <T>
     */
    private <T> void field(Map<String, Class<T>> annotaionClassMap) {
        annotaionClassMap.forEach((classPath, clazz)->{
            Field[] fields = clazz.getDeclaredFields();
            if (fields!=null) {
                for (int i = 0; i < fields.length; i++) {
                    Field field = fields[i];
                    if (field.isAnnotationPresent(Autorwired.class)) {
                        Class clz = field.getType();
                        Object fieldObject = CLASS_OBJECT_CONTAINER.get(clz.getName());
                        if (fieldObject!=null) {
                            Object classObject = CLASS_OBJECT_CONTAINER.get(clazz.getName());
                            try {
                                field.setAccessible(true);
                                field.set(classObject, fieldObject);
                            } catch (IllegalAccessException e) {
                                e.printStackTrace();
                            }
                        } else {
                            //先不考虑这种情况哈
                        }
                    }
                }
            }
        });
    }

    /**
     * 先实例化对象
     * @param annotaionClassMap
     * @param <T>
     */
    private <T> void instance(Map<String, Class<T>> annotaionClassMap) {
        annotaionClassMap.forEach((classPath, clazz)->{
            try {
                T t = clazz.newInstance();
                CLASS_OBJECT_CONTAINER.put(classPath, t);
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        });
    }

    public Map<Method, Object> getFunMap(HttpRequestType type, String api) {
        Map<String, Map<Method, String>> apiMethodObjectMap = HTTP_API_CONTAINER.get(type);
        if ( null == apiMethodObjectMap ) {
            return null;
        }

        Map<Method, Object> map = new ConcurrentHashMap<>();
        apiMethodObjectMap.get(api).forEach(((method, classPath) -> {
            map.put(method, CLASS_OBJECT_CONTAINER.get(classPath));
        }));

        return map;
    }
}
