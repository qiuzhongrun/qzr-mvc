package qiuzhongrun.com.controller;

public interface BaseController {
    String say();
    String make(String name);
}

