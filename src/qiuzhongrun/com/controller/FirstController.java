package qiuzhongrun.com.controller;

import qiuzhongrun.com.annotation.Autorwired;
import qiuzhongrun.com.annotation.Controller;
import qiuzhongrun.com.annotation.MethodLog;
import qiuzhongrun.com.annotation.RestMapping;
import qiuzhongrun.com.service.FirstService;

import static qiuzhongrun.com.enumeration.HttpRequestType.GET;
import static qiuzhongrun.com.enumeration.HttpRequestType.POST;

@Controller
@MethodLog
public class FirstController implements BaseController{
    @Autorwired
    private FirstService firstService;

    @RestMapping(api = "/say", type = GET)
    public String say() {
        return firstService.getWords();
    }

    @RestMapping(api = "/make", type = POST)
    public String make(String name) {
        return firstService.make(name);
    }
}
