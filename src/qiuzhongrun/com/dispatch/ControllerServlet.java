package qiuzhongrun.com.dispatch;

import qiuzhongrun.com.container.Container;
import qiuzhongrun.com.enumeration.HttpRequestType;
import qiuzhongrun.com.util.ScanUtil;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class ControllerServlet extends HttpServlet {
    private Container container;
    private String servletName;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("处理Get请求。。。。。");
        String output = invoke(req, resp, HttpRequestType.GET);
        PrintWriter out = resp.getWriter();
        out.println(output);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("处理Post请求。。。。。");
        PrintWriter out = resp.getWriter();
        String output = invoke(req, resp, HttpRequestType.POST);
        out.println(output);
    }


    private String invoke(HttpServletRequest req, HttpServletResponse resp, HttpRequestType type) throws IOException {
        String api = req.getServletPath();
        api = api.substring(servletName.length() + 1);

        Map<Method, Object> funMap = container.getFunMap(type, api);

        if (null == funMap) {
            resp.sendError(501, api);
        }

        Map<String, String[]> parameterMap = req.getParameterMap();
        int paramLen = parameterMap.size();
        String[] args = new String[paramLen];//先只考虑字符串的，还没做参数自动转化, 也没考虑顺序

        if (paramLen > 0) {
            int i = 0;
            Iterator<Map.Entry<String, String[]>> entries = parameterMap.entrySet().iterator();
            while (entries.hasNext()) {
                Map.Entry<String, String[]> entry = entries.next();
                args[i++] = entry.getValue()[0];
            }
        }

        //这里存在多个参数的问题，我们先只考虑只存在一种参数列表的情况
        //也就是map里只有一对
        StringBuffer stringBuffer = new StringBuffer();
        Iterator<Map.Entry<Method, Object>> it = funMap.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<Method, Object> entry = it.next();
            Method targetMethod = entry.getKey();
            Object o = entry.getValue();
            try {
                Method[] methods = o.getClass().getDeclaredMethods();
                for (int i = 0; i < methods.length; i++) {
                    Method method = methods[i];
                    if (isTheSameMethod(targetMethod, method)) {
                        stringBuffer.append(method.invoke(o, args));
                    }
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }
        }

        return stringBuffer.toString();
    }

    private boolean isTheSameMethod(Method targetMethod, Method method) {
        if (targetMethod.getName() == method.getName()) {
            if (targetMethod.getReturnType() == method.getReturnType()) {
                if (targetMethod.getParameterCount() == method.getParameterCount()) {
                    return true;
                }
            }
        }

        return false;
    }


    @Override
    public void init() throws ServletException {
        //不好意思，我要开启mvc之旅了
        System.out.println("不好意思，我要开启mvc之旅了");

        try {
            // 1 扫描注解
            Set<Class> classes = ScanUtil.scanClasses(this.getClass());

            // 2 初始化容器
            container = new Container();
            container.init(classes);

            servletName = this.getServletConfig().getServletName();
            super.init();

            System.out.println("mvc完成，尽情享用吧");
        } catch (Throwable e) {
            System.err.println("Init failed" + e.getMessage());
        }
    }
}
