package qiuzhongrun.com.service;

import qiuzhongrun.com.annotation.Service;

@Service
public class FirstService {
    private String words = "YOU GOT IT!";

    public String getWords() {
        return words;
    }

    public String make(String name) {
        return name + " YES! YOU MADE IT.";
    }
}
