package qiuzhongrun.com.util;

import java.io.File;
import java.util.HashSet;
import java.util.Set;

public class ScanUtil {

    public static void scanClasses(String classPath, String pkgPath, Set<Class> classes) {
        File dir = new File(pkgPath);
        if (!dir.exists() || !dir.isDirectory()) {
            return;
        }

        // 过滤获取目录，或者 class文件
        File[] dirfiles = dir.listFiles(pathname -> pathname.isDirectory() ||   pathname.getName().endsWith("class"));

        if (dirfiles == null || dirfiles.length == 0) {
            return;
        }

        String className;
        Class clz;
        for (File f : dirfiles) {
            if (f.isDirectory()) {
                scanClasses(classPath + "." + f.getName(),
                        pkgPath + "/" + f.getName(),
                        classes);
                continue;
            }

            // 获取类名，去除 ".class" 后缀
            className = f.getName();
            className = className.substring(0, className.length() - 6);

            // 加载类
            String loadclass = classPath + "." + className;

            //调用类加载器
            clz = loadClass(loadclass);
            if (clz != null) {
                classes.add(clz);
            }
        }
    }

    //类加载器
    public static Class<?> loadClass(String fullClzName) {
        try {
            return Thread.currentThread().getContextClassLoader().loadClass(fullClzName);
        } catch (Throwable e) {
            System.err.println("clz: " + fullClzName);
            e.printStackTrace();
        }
        return null;
    }

    public static Set<Class> scanClasses(Class clazz) {
        String classFilePath = clazz.getResource("").getPath();
        String packagePath =classFilePath.substring(0, classFilePath.length()-9);
        Set<Class> classes = new HashSet<>();
        scanClasses(clazz.getName().substring(0, clazz.getName().length()-".dispatch.ControllerServlet".length()), packagePath, classes);
        return classes;
    }
}
